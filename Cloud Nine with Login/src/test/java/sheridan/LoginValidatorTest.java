package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginCharactersRegular( ) {
		boolean isLoginValid = LoginValidator.isValidLoginName("harmanbath123");
		assertTrue("Invalid login" , isLoginValid );
	}
	
	@Test
	public void testIsValidLoginCharactersBoundaryIn() {
		boolean isLoginValid = LoginValidator.isValidLoginName("hbath1");
		assertTrue("Invalid login" , isLoginValid );

	}
	
	@Test
	public void testIsValidLoginCharactersBoundaryOut() {
		boolean isLoginValid = LoginValidator.isValidLoginName("hbath!");
		assertFalse("Invalid login" , isLoginValid );
	}
	
	@Test
	public void testIsValidLoginCharactersExceptional() {
		boolean isLoginValid = LoginValidator.isValidLoginName("123$@4%");
		assertFalse("Invalid login" , isLoginValid );
	}
	
	@Test
	public void testIsValidLoginLengthRegular( ) {
		boolean isLoginValid = LoginValidator.isValidLoginName("harman123bath");
		assertTrue("Invalid login" , isLoginValid );
	}

	@Test
	public void testIsValidLoginLengthBoundaryIn() {
		boolean isLoginValid = LoginValidator.isValidLoginName("bath12");
		assertTrue("Invalid login" , isLoginValid );

	}
	
	@Test
	public void testIsValidLoginLengthBoundaryOut() {
		boolean isLoginValid = LoginValidator.isValidLoginName("hbath");
		assertFalse("Invalid login" , isLoginValid );
	}
	
	@Test
	public void testIsValidLoginLengthExceptional() {
		boolean isLoginValid = LoginValidator.isValidLoginName("hb1");
		assertFalse("Invalid login" , isLoginValid );
	}
	
	
	


	
}
